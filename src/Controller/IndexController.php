<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\LoginType;
use App\Form\RegistrationType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Repository\UserRepository;
use Curl\Curl;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class IndexController extends Controller
{
    /**
     * @Route("/", name="app_home")
     */
    public function indexAction(ApiContext $context)
    {

        return $this->render('index.html.twig', []);
    }

    /**
     * @Route("/social-auth-back", name="app_social_auth_back")
     */
    public function socialAuthBackAction(ApiContext $context)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($s, true);
        //$user['network'] - соц. сеть, через которую авторизовался пользователь
        //$user['identity'] - уникальная строка определяющая конкретного пользователя соц. сети
        //$user['first_name'] - имя пользователя
        //$user['last_name'] - фамилия пользователя

        dump($user);

        return $this->render('index.html.twig', []);
    }

    /**
     * @Route("/make-ping", name="app_make_ping")
     */
    public function makePingAction(ApiContext $context)
    {
        try {
            return new Response(var_export($context->makePing(), true));
        } catch (ApiException $e) {
            return new Response($e->getMessage());
        }
    }

    /**
     * @Method({"POST", "GET"})
     * @Route("/registration")
     * @param Request $request
     */
    public function registrationAction(Request $request)
    {
        $user = new User();

        $form = $this->createForm(RegistrationType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();


            $userData = $form->getData();

            $data = [
                    'email' => $userData->getEmail(),
                    'idPassport' => $userData->getIdPassport(),
                    'password' => $userData->getPassword()
            ];

            $verified = $this->verificationUser($data);

            if(!empty($verified)){
                $isExist = true;
                return $this->render('warning.html.twig', ['userExist' => $isExist]);
            }

            $em->persist($userData);
            $em->flush();
        }

        return $this->render('registration.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    /**
     * @Route("/login", name="app_login_page")
     * @param Request $request
     * @param UserRepository $repository
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function loginAction(Request $request, UserRepository $repository)
    {
        $currentUser = new User();

        $form = $this->createForm(LoginType::class, $currentUser);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userData = $form->getData();

            $user = [
                'email' => $userData->getEmail(),
                'password' => $userData->getPassword(),
                'roles' =>$userData->getRoles(),
            ];



            $resultFromLocalSqlData = $repository->findUserOnLocalServer($user);

            if(!empty($resultFromLocalSqlData)){

                $token = new UsernamePasswordToken($currentUser, null, 'main', $resultFromLocalSqlData[0]->getRoles());
                $this
                    ->container
                    ->get('security.token_storage')
                    ->setToken($token);
                $this
                    ->container
                    ->get('session')
                    ->set('_security_main', serialize($token));

                return $this->showUserDataIfHeDidLogin($user['email']);
            }


            $resultFromMainSqlData = $this->tryFindExistClientOnMainServer($user);

            if(!empty($resultFromMainSqlData)){
                $resultFromMainSqlData1 = json_decode(json_encode($resultFromMainSqlData), true);

                $userDataFromMainServer = [
                    'email'      => $resultFromMainSqlData1[0]['email'],
                    'password'   => $resultFromMainSqlData1[0]['password'],
                    'idPassport' => $resultFromMainSqlData1[0]['id_passport']
                ];

                $this->addClientFromMainServerToLocalServer($userDataFromMainServer);

                $token = new UsernamePasswordToken($currentUser, null, 'main', $resultFromMainSqlData[0]->getRoles());
                $this
                    ->container
                    ->get('security.token_storage')
                    ->setToken($token);
                $this
                    ->container
                    ->get('session')
                    ->set('_security_main', serialize($token));

                return $this->showUserDataIfHeDidLogin($userDataFromMainServer['email']);
            }

            return new Response('Неверный логин или пароль');
        }
        return $this->render('login.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/auth", name="auth")
     */
    public function authAction(UserRepository $userRepository)
    {
        $user = $userRepository->find(1);
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this
            ->container
            ->get('security.token_storage')
            ->setToken($token);
        $this
            ->container
            ->get('session')
            ->set('_security_main', serialize($token));
        return new Response('Вы авторизованы');
    }

    /**
     * @Route("/welcome-user", name="welcome")
     */
    public function showUserDataIfHeDidLogin($name)
    {
        return $this->render('innerpage.html.twig', array(
            'name' => $name
        ));
    }

    public function verificationUser($data)
    {
        $userVerification = new Curl();
        $userVerification->get('http://127.0.0.1:8000/verification-client', [
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdhbml6YXRpb25OYW1lIjoiOlx1MDQxYlx1MDQzMFx1MDQzMlx1MDQzYVx1MDQzMCBcdTA0M2ZcdTA0NDBcdTA0MzhcdTA0M2FcdTA0M2JcdTA0NGVcdTA0NDdcdTA0MzVcdTA0M2RcdTA0MzhcdTA0MzkiLCJvcmdhbml6YXRpb25TaXRlVVJMIjoiOnNvbWUuY29tIiwiaWQiOjF9.CZejIOw_jIX0xDSMU4lcbV5QVlBBGnnzQWvt1rBucTk',
                'userData' => $data,
        ]);

        if ($userVerification->error) {
            return $userVerification->response = 'Error: ' . $userVerification->errorCode . ': ' . $userVerification->errorMessage . "\n";
        } else {
            return $userVerification->response;
        }
    }

    public function tryFindExistClientOnMainServer($data)
    {
        $userVerification = new Curl();
        $userVerification->get('http://127.0.0.1:8000/try-login', [
            'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdhbml6YXRpb25OYW1lIjoiOlx1MDQxYlx1MDQzMFx1MDQzMlx1MDQzYVx1MDQzMCBcdTA0M2ZcdTA0NDBcdTA0MzhcdTA0M2FcdTA0M2JcdTA0NGVcdTA0NDdcdTA0MzVcdTA0M2RcdTA0MzhcdTA0MzkiLCJvcmdhbml6YXRpb25TaXRlVVJMIjoiOnNvbWUuY29tIiwiaWQiOjF9.CZejIOw_jIX0xDSMU4lcbV5QVlBBGnnzQWvt1rBucTk',
            'userData' => $data,
        ]);

        if ($userVerification->error) {
            return $userVerification->response = 'Error: ' . $userVerification->errorCode . ': ' . $userVerification->errorMessage . "\n";
        } else {
            return $userVerification->response;
        }
    }



    /**
     * @param array $data
     */
    public function addClientFromMainServerToLocalServer(Array $data)
    {
        $user = new User();

        $user->setEmail($data['email']);
        $user->setIdPassport($data['idPassport']);
        $user->setPassword($data['password']);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
    }

}
